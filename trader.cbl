       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER-REPORT.
       AUTHOR. MONGKOL
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT TRADER-REPORT-FILE ASSIGN TO "trader.rpt"
              ORGANIZATION IS  SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER-REPORT-FILE.
       01 PRINT-LINE              PIC X(44).

       FD  TRADER-FILE
       01 MEMBER-REC.
          88 END-OF-TRADER-FILE             VALUE HIGH-VALUE.
       05 TRADER-PROVINCE         PIC X(4).
          05 TRADER-INCOME        PIC X(9).
          05 TRADER-MEMBER        PIC X(4).
          05 TRADER-PRICE         PIC X(6).
       WORKING-STORAGE SECTION.
       01 COLUMN-HEADING          PIC X(51)
                                            VALUE
             "PROVINCE      P INCOME      MEMBER   MEMBER INCOME".
       01 TRADER-DETAIL-LINE.
          05 FILLER               PIC X     VALUE SPACES.
          05 PRN-TRADER-PROVINCE  PIC X(99).
          05 FILLER               PIC X(4)  VALUE SPACES.
          05 PRN-TRADER-INCOME    PIC X(99).
          05 FILLER               PIC X(5)  VALUE SPACES.
          05 PRN-TRADER-MEMBER    PIC X.
          05 FILLER               PIC X(10) VALUE SPACES.
          05 PRN-TRADER-PRICE     PIC X(6).
       01 LINE-COUNT                        VALUE ZEROES.
          88 NEW-PAGE-REQIRED               VALUE 40 THRU 99.
       01 PAGE-COUNT              PIC 99    VALUE ZEROES.
       PROCEDURE DIVISION .

       PROCESS-TRADER-REPORT.
           OPEN INPUT TRADER-FILE 
           OPEN OUTPUT TRADER-REPORT-FILE 
           
           
           CLOSE TRADER-FILE, TRADER-REPORT-FILE 
           GOBACK 
           .
       WRITE-HEADING.
           WRITE PRINT-LINE FROM COLUMN-HEADING AFTER ADVANCING 2 
           COMPUTE LINE-COUNT = LINE-COUNT + 3
           COMPUTE PAGE-COUNT = PAGE-COUNT + 1
           MOVE ZEROS TO LINE-COUNT 
           .
       PROCESS-DETATL.
           MOVE TRADER-PROVINCE TO PRN-TRADER-PROVINCE
           MOVE TRADER-INCOME TO PRN-TRADER-INCOME
           MOVE TRADER-MEMBER TO PRN-TRADER-MEMBER 
           MOVE TRADER-PRICE TO PRN-TRADER-PRICE 
           WRITE PRINT-LINE FROM TRADER-DETAIL-LINE
              AFTER ADVANCING 1 LINE 
           COMPUTE LINE-COUNT = LINE-COUNT + 1
           .
       READ-TRADER-FILE.
           READ TRADER-FILE 
           AT END
              SET END-OF-TRADER-FILE TO TRUE 
           END-READ
           .